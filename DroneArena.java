package drone_project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DroneArena {
	
	private int xmax, ymax, drone_x, drone_y;
	Random randomGenerator;
	public ArrayList<Drone> manyDrones;
	
	public DroneArena(int i,int j){
		xmax = i;
	    ymax = j;
	    manyDrones = new ArrayList<Drone>();
	    }
	    
	public int getXmax() {
	    return xmax;
	    }

	 public int getYmax() {
	    return ymax;
	    }

	 public void setXYmax(int xmax, int ymax) {
		this.xmax = xmax;
	    this.ymax = ymax;
	    }
	 
	 public String toString(){
		 String s = "Arena is " + xmax +" by "+ymax+"\n";
	     for(Drone d: manyDrones){
	    	 s = s + d.toString() + "\n";
	    	 }
	     return s;
	     }
	    
	 public void showDrones(ConsoleCanvas c) {
		 for(Drone d: manyDrones){
			 d.displayDrone(c);
		 }
		 
	 }
	    
	 public void showUpdatedDrones(ConsoleCanvas c) {
	        for(Drone d: manyDrones){
	           d.displayUpdateDrone(c);
		  }
	     
	    
	    }
	    public void moveAllDrones(){
	       for(Drone d: manyDrones){       
	           d.tryToMove(this);
		  }
	    }
	    
	    public void addDrone(){
	    	/*randomGenerator = new Random();
	        drone_x = randomGenerator.nextInt(xmax);
	        drone_y = randomGenerator.nextInt(ymax);       
	        Direction.DirEnum x = Direction.DirEnum.getRandomDirection();
	        manyDrones.add(new Drone(drone_x,drone_y,x));*/
	    	randomGenerator = new Random();
	    	boolean check = true;
	        while(check){
	        drone_x = randomGenerator.nextInt(xmax);
	        drone_y = randomGenerator.nextInt(ymax);
	        if(drone_x>0 && drone_x<xmax-2 && drone_y>0 && drone_y<ymax-2 && isHere(drone_x,drone_y)==false){
	        Direction.DirEnum x = Direction.DirEnum.getRandomDirection();
	        manyDrones.add(new Drone(drone_x,drone_y,x));
	        check = false;
	        }
	        }
	    }

	    public boolean canMoveHere(int nx,int ny ){
	          if(nx>0 && nx<xmax-2  && ny>0 && ny<ymax-2 && isHere(nx,ny)==false){
	              return true;  
	          }
	          else{
	          return false;
	          }  
	    }
	    
	    public boolean isHere (int sx, int sy) {
	        int a = 0;
	            for(Drone d: manyDrones){
	            	if((sx==d.getX())&&(sy == d.getY())){
	            		++a;
	            	}else {
	            		continue;
	            	}
	            }
	            if(a>0) {
	            	return true;
	            	}else {
	            		return false;
	            	}
	    }
	    
	   public void setArena(String [] aI) {
	    String[] str = aI;
	    //Removing every drone from arena in order to et the pre stored drone back in arena
	    this.manyDrones.clear();
	    
	    int i = 0;
	    for (String st: str) {    
	    
	    if (i == 0){
	        st = st.replaceAll("[^0-9]+", " ");
	        List<String> list=Arrays.asList(st.trim().split(" "));
	        this.xmax = Integer.parseInt(list.get(0));
	        this.ymax = Integer.parseInt(list.get(1));
	              
	        //System.out.println(list);
	    }
	    else{
	        //System.out.println("Now in else");
	        String test = st;
	        st = st.replaceAll("[^0-9]+", " ");
	        List<String> list=Arrays.asList(st.trim().split(" "));
	        drone_x = Integer.parseInt(list.get(1));
	        drone_y = Integer.parseInt(list.get(2));
	         
	        if(test.contains("North")){
	            Direction.DirEnum x = Direction.DirEnum.North;
	            this.manyDrones.add(new Drone(drone_x,drone_y,x));
	            
	        }
	            //System.out.println("North");}
	        else if(test.contains("South")){
	            Direction.DirEnum x = Direction.DirEnum.South;
	            this.manyDrones.add(new Drone(drone_x,drone_y,x));
	            
	            //System.out.println("South");
	                }
	        else if(test.contains("East")){
	            Direction.DirEnum x = Direction.DirEnum.East;
	            this.manyDrones.add(new Drone(drone_x,drone_y,x));
	            //System.out.println("East");
	        }
	        else if(test.contains("West")){
	            Direction.DirEnum x = Direction.DirEnum.West;
	            this.manyDrones.add(new Drone(drone_x,drone_y,x));
	            //System.out.println("West");
	        }
	    }
	    i++;
	    }
	    }             
	    
	    public static void main(String[] args) {
	        // TODO code application logic here
	        DroneArena da = new DroneArena(24,24);
	        da.addDrone();
	        da.addDrone();
	        da.addDrone();
	        System.out.println(da.toString());
	        da.isHere(5,5);	        
	    }
}
