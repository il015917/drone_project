package drone_project;

public class Drone {
	private int x, y, droneID, dx, dy;
	private static int DroneCount = 0;
	Direction.DirEnum direction;

	public int getX() {
		return x;
		}

	public int getY() {
	      return y;
	      }

	public void setXY(int nx, int ny){
	       this.x = nx;
	       this.y = ny;
	       }
	
	public String toString(){
	    return "Drone "+ droneID + " at " + x +","+y+" in "+direction + " direction ";
	    }
	
	public Drone (int bx,int by,Direction.DirEnum dir){
		x = bx;
	    y = by;
	    droneID = DroneCount++;
	    direction = dir;
	    dx = 1;
	    dy = 1;
	    }
	
	public void displayDrone(ConsoleCanvas c) {
	    c.showIt(x, y, 'D');
	    }
	    
	public void displayUpdateDrone(ConsoleCanvas c){
	    c.updateCanvas(x, y, c);
	    c.showIt(x, y, 'D');    
	    }
	
	public void tryToMove(DroneArena d){
		int nx=0;int ny =0;
	    if(direction == Direction.DirEnum.North){
	    	nx = x+0;
	        ny = y+dy;
	        }
	        
	    if(direction == Direction.DirEnum.South){
	        nx = x+0;
	        ny = y-1;
	        }
	        
	    if(direction == Direction.DirEnum.East){
	        nx = x+dx;
	        ny = y+0;
	        }
	       
	    if(direction == Direction.DirEnum.West){
	        nx = x-1;
	        ny = y+0;
	        }
	    
	    boolean status = d.canMoveHere(nx,ny);
	    if(status == true){
	        this.x=nx;
	        this.y=ny; 
	        }else{
	        	direction = direction.getNextDirection(direction);
	        }
	    }
	
	public static void main(String[] args) {
		// TODO code application logic here
		Drone d1 = new Drone(8,12,Direction.DirEnum.North);   
		System.out.println(d1.toString());
	    Drone d2 = new Drone(16,24,Direction.DirEnum.East);            
	    System.out.println(d2.toString());
	    }
}

