package drone_project;

import java.util.Random;

public class Direction {
	public enum DirEnum {
		North,South,East,West;
	    
	    public static DirEnum getRandomDirection() {
	            Random random = new Random();
	            return values()[random.nextInt(values().length)];
	        }
	    
	     public DirEnum getNextDirection(DirEnum x){
	    	if (x == DirEnum.North){
	    		return DirEnum.East;
	    	}
	    	else if (x == DirEnum.East){
	    		return DirEnum.South;
	    	}
	    	else if (x == DirEnum.South){
	    		return DirEnum.West;
	    	}
	    	else if (x == DirEnum.West){
	    		return DirEnum.North;
	    	}
	    	return DirEnum.North;
	     }
	}
	    
	public static void main(String[] args) {
		DirEnum x = DirEnum.getRandomDirection();
	    System.out.println(x);
	    System.out.println("if "+x+" Next "+x.getNextDirection(x));
	}
}
