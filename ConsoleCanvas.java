package drone_project;

public class ConsoleCanvas {
	 private int rows;
	 private int cols;
	 char [][] canvas;
	 
	 public ConsoleCanvas(int nx,int ny){
		 rows = nx;
	     cols = ny;
	     canvas = new char[rows][cols];
	     
	     for(int x = 0; x < rows; x++) {
	    	 for(int y = 0; y < cols; y++) {
	    		 if(x==0 || y==0  || x==rows-1 || y==cols-1) {
	    			 canvas[x][y] = '#';		
	    			 }else {
	    				 canvas[x][y] = ' ';
	    				 }
	    		 }
	    	 }
	     }
	    
	    public String toString(){
	         for(int x = 0; x < rows; x++){
	            for(int y = 0; y < cols; y++){
	                  System.out.print(canvas[x][y]);
	                  }
	            System.out.println();
	            }
	         return " ";
	         }
	    	    
	    public void showIt(int x ,int y,char d){
	        if(canvas[x][y]==' ' && canvas[x][y]!='#'){
	            canvas[x][y] = d;
	        }
	    }
	    
	    public void updateCanvas(int x,int y,ConsoleCanvas c){
		       if(c.canvas[x-1][y-1]!='#' && x>0 && y>0){
		            c.canvas[x-1][y-1] = ' ';
		       }
		    
		    }
	    
	    public static void main(String[] args){
	    ConsoleCanvas cc = new ConsoleCanvas(5,10);
	    cc.showIt(1,2,'D');
	    cc.toString();	     
	    }
}
