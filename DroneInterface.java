package drone_project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.swing.JFileChooser;

public class DroneInterface {
    int file_name = 0;
    private Scanner s;  
    private DroneArena myArena;   
    private static Scanner scanner = new Scanner( System.in );
   
    public void doDisplay() {				//display drones
    	int x = myArena.getXmax();
        int y = myArena.getYmax();
        ConsoleCanvas c = new ConsoleCanvas(x,y);
        myArena.showDrones(c);
        c.toString();
    }
    
    public void moveAllDrones() {			//move drones
        int x = myArena.getXmax();
        int y = myArena.getYmax();
        ConsoleCanvas c = new ConsoleCanvas(x,y);
        myArena.moveAllDrones();
        myArena.showUpdatedDrones(c);
        c.toString();
    }
    
    public DroneInterface() {
    	s = new Scanner(System.in);  
        System.out.print( "Input length of Arena: " );
        int X = scanner.nextInt();
        System.out.print( "Input height of Arena: " );
        int Y = scanner.nextInt();
        myArena = new DroneArena(X, Y);// custom arena
        
        char ch = ' ';
        do { 
            System.out.print("Enter (A)dd drone, (I)nformation , (D)isplay Drone , Move (T)en times, (M)ove Drone and (F)ile Handling or e(X)it > ");
            ch = s.next().charAt(0);
            s.nextLine();
            switch (ch) {
                    case 'A' :
                    case 'a' :
                                    myArena.addDrone();    // add a new drone to arena
                                    break;
                    case 'I' :
                    case 'i' :
                                    System.out.print(myArena.toString());
                                    break;
                    case 'M' :
                    case 'm' :
                    				myArena.moveAllDrones();
                    				doDisplay();
                    				break;
                    				
                    case 'T' :
                    case 't' :
                                    for(int i = 0 ;i<10;i++){ 		//move all drones
                                    	try {
                                    		TimeUnit.MILLISECONDS.sleep(250);
                                    		moveAllDrones();
                                    		}catch (InterruptedException ie) {
                                    			Thread.currentThread().interrupt();
                                    		} 
                                    } 
                                    break;
                    case 'f':
                    case 'F':
                    				int s = choosefileoption();	 	//Choosing option for file (read or write)
                    				if(s == 1) {
                    					File_management(1);			//Saving to file func
                    					}else {
                    						String str = File_management(2);
                    						String lines[] = str.split("\n");
                    						myArena.setArena(lines);           
                                        	}
                    				break;
                    case 'D' :
                    case 'd' :
                                 	doDisplay();
                                 	break;
                    case 'x' :     ch = 'X';                // when X detected program ends
                                    break;
            		}
            }while (ch != 'X');                        		// test if end
        s.close();                                    		// close scanner
    }
    
    private int choosefileoption() {									//Option for file (Loading or saving)
	   s = new Scanner(System.in);										// set up scanner for user input
	   
	   System.out.println("1 - Save to file current Building info");	//Instruction 1 output
	   System.out.println("2 - Load Building from file");				//Instruction 2 output
	   System.out.print("Selection: ");
	   int n = s.nextInt();												//Getting input int
	   
	   return n;														//Returning input
   }
    
   private String File_management(int n) {								//File management function
	   JFileChooser chooser = new JFileChooser();						//Declaring File chooser
	   
	   if(n == 1) {														//If saving option is true
		  System.out.println("FILE SAVER OPEN BEHIND MAIN WINDOW");
	   int returnValue = chooser.showSaveDialog(null);
       if (returnValue == JFileChooser.APPROVE_OPTION) {
         File selectedFile = chooser.getSelectedFile();					//Creating new file
         if(selectedFile.canWrite()) {									//Checking if writing permission is true
        	 try {														//Try func to catch exceptions
        		 FileWriter file_writer = new FileWriter(chooser.getSelectedFile());	//Assign file to file_writer
        		 file_writer.write(myArena.toString());								//Writing to buffer building string
        		 file_writer.close();													//Closing file writer to write from buffer
        		 System.out.println("BUILDING INFO CORRECTLY SAVED TO FILE");			//Output result if successful
        	 }
        	 catch(Exception ex){														//Exception catch
        		 System.out.println("ERROR ENCOUNTERED WHILE WRITING TO FILE");			//Print error message (prevent crash)
        		 ex.printStackTrace();													//Print error in console
        	 }
        	 
         }
       }
	   }else if(n == 2) {																//File opener option (reading building string)
		   System.out.println("FILE OPENER OPEN BEHIND MAIN WINDOW");					//File opener open behind main window
		   
		   int returnValue = chooser.showOpenDialog(null);
	       if (returnValue == JFileChooser.APPROVE_OPTION) {
	         File selectedFile = chooser.getSelectedFile();								//New file assigned to selected file
	         if(selectedFile.canRead()) {												//Checking read permission
	        	 try {																	//Try function to catch exception
	        		 BufferedReader file_reader = new BufferedReader(new FileReader(chooser.getSelectedFile()));		//Buffered read
	        		 String arena_info = "";										//Building read string
	        		 String file_info = "";
                                  while ((file_info = file_reader.readLine()) != null){ 
                                    //System.out.println(line + "1");
                                        arena_info += file_info;
                                        arena_info +='\n';
                                        } 
                                 
	        		 file_reader.close();												//Closing reader to save to memory instead of buffer
	        		 System.out.println("BUILDING INFO CORRECTLY LOADED FROM FILE");
	        		 return arena_info;											//Returning read string
	        	 }
	        	 catch(Exception ex){													//Catching exception (prevent crash)
	        		 System.out.println("ERROR ENCOUNTERED WHILE OPENING TO FILE");
	        		 ex.printStackTrace();												//Printing to console exception (crash)
	        	 }
	        	 
	         }
	       }
	   }
	   
	return null;														//Unreachable code to prevent error

   }
   
    public static void main(String[] args) {
        // TODO code application logic here
        DroneInterface r = new DroneInterface();
        
    }
    
}
